*This is a React Native take-home style assignment for tychon candidates*

# Exercise 1 – Logic Programming

## 1. Part A

By starting at the top of the triangle and moving to adjacent numbers on the row below, the maximum total from top to bottom is 27.

       5 
      9 6
     4 6 8  
    0 7 1 5  

i.e. 5 + 9 + 6 + 7 = 27.

Write a program using a language of your choice to find the maximum total from top to bottom in a text file containing a triangle with 100 rows.

The data file can be accessed [here](https://gist.github.com/kevinbt/c0a4e0ce00edb2e8b4bc72aa4dc9e37a).

## 2. Part B

The sentence ***`A quick brown fox jumps over the lazy dog`*** contains every single letter in the alphabet. Such sentences are called pangrams.

You are to write a method `GetMissingLetters` using a language of your choice, which takes a String, a sentence, and returns all the letters it is missing (which prevent it from being a pangram).

You should ignore the case of the letters in sentence, and your return should be all lower case letters, in alphabetical order.
You should also ignore all non US-ASCII characters.

The code you submit must contain a method that conforms to the expected method signature, as follows:

    Public Class: MissingLetters

    Method signature: public String GetMissingLetters(String sentence)

# Exercise 2 – To Do Application

The goal of this task is to create a mobile app using React Native and Expo (optional) that can manage to do items. We would like you to demonstrate you are familiar with the technologies we use at tychon by solving the task described below.

Setup your Development Environment [(React Native - Getting Started guide)](https://facebook.github.io/react-native/docs/getting-started.html) and create a Git repository that will be used to submit your code.

## Required Features 
 
- Simple Authentication/Passcode protection
- Add a new Todo item.
- Recurring Todo items.
- Mark Todo item as done
- Delete a Todo item
- Edit a Todo item
- Pictures on Todo item
- Allow Todo items to have prioritization
- Display number of Todo items left to be completed
- Filter between completed and active Todo items, today’s Todo items, future Todo items, overdue tasks.
- Search Todo items.
- Sort Todo items by date/time created, date/time completed.

## Requirements
- Persist data using Redux (or another state container e.g. MobX)
- Use Typescript
- Show applicable use of React hooks
- Use Test automation (ex. Jest)

## Bonus Points Awarded For
- Use of animation
- Use of an API to retrieve and save Todo items
- Alert user about overdue Todo items using local push notifications


Within these parameters there are still many variations to solve the task. If necessary, you can make (and note) assumptions about the data and the interpretation of the task.


## Evaluation
1. We were able to easily build and run your code.
2. Code quality - is your code clean, simple, commented, modern, well designed.
3. User experience - how simple, intuitive and clear is the UI
4. UI – using a well designed layout that can have a consistent look and feel across devices.
5. Error handling - your code should handle potential errors gracefully.

# Submission Instructions

Commit your final version to your own Github repository and email your repository link containing your solution.
